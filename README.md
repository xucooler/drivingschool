<<<<<<< HEAD
# 驾校小程序

#### 介绍
驾校约车微信小程序

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
=======
# 驾校学车微信小程序（学员端）
这是一款用于学车平台为学员提供学车报名、预约练车服务的微信小程序  
This is a mini program project of WeChat  

![license](https://img.shields.io/github/license/EpearthLtd/drivingSchool-mini-student.svg)
![GitHub release](https://img.shields.io/github/release/EpearthLtd/drivingSchool-mini-student/all.svg)
![Github commits (since latest release)](https://img.shields.io/github/commits-since/EpearthLtd/drivingSchool-mini-student/latest.svg)  

## 部分界面截图
<img src="/demoImg/training.jpg" alt="page training (home page)" width="33%"> <img src="/demoImg/poster.jpg" alt="page poster" width="33%"> <img src="/demoImg/me.jpg" alt="page me" width="33%"> 
<img src="/demoImg/reservation.jpg" alt="page reservation" width="33%"> <img src="/demoImg/coachinfo.jpg" alt="page coachinfo" width="33%"> 

## 技术参数
* 调试基础库：2.2.2
* 业务域名：https://develop.epearth.com

## 设计特色
* 全局变量设置根域名、资源域名、APP名称，简化变更小程序信息的步骤

## 设计功能模块
* 【已发布】轮播广告图
* 【已发布】在线客服（原生功能）
* 【已发布】预约练车
* 【已发布】记录查询
* 【已发布】手机绑定
* 【已发布】意见及BUG反馈（原生功能）
* 【已发布】拨打电话

## 基础系统
* 全局根域名
* 报名状态系统
* 预约及练车状态系统
* 练车进度系统

## 版本说明
* V1 静态海报广告：只包含广告展示和客服模块，没有约车功能
* V2 预约练车：包含预约练车的功能模块（开发中）

## 命名书写方法
* 小驼峰：页面文件命名、变量命名、参数命名
* 短横线：wxss命名、资源文件命名
```JavaScript
var thisIsExample   // 小驼峰变量参数命名示例
onShareAppMessage: function () {
                    // 小驼峰函数命名示例
},
```
```CSS
.color-bg-gray {    <!--短横线分隔命名示例-->
  background-color: #CCC;
}
```
## 链接
* [WeUI](https://github.com/Tencent/weui-wxss)





>>>>>>> init
