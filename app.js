/*!
 * 作者author：刘旭，杨正宇
*/

App({
  onLaunch: function () {
    // 展示本地存储能力
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)

    // 获取当前
    var date = new Date();
    var dateArray = [];
    for (var i = 0; i < 4; i++) {
      var mDate = (date.getMonth() + 1) + '月' + (date.getDate() + i) + '日';
      dateArray.push(mDate);
    }
    this.globalData.dateList = dateArray;
    console.log('全局数组dateArray为：' + this.globalData.dateList);
    // 登录
    wx.login({
      success: function(res) {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
        // 获取openid
        // var code = res.code; //返回code
        // console.log(code);
        // var appId = '	***';
        // var secret = '***';
        // wx.request({
        //   url: 'https://api.weixin.qq.com/sns/jscode2session?appid=' + appId + '&secret=' + secret + '&js_code=' + code + '&grant_type=authorization_code',
        //   data: {},
        //   header: {
        //     'content-type': 'json'
        //   },
        //   success: function (res) {
        //     var openid = res.data.openid //返回openid
        //     console.log('openid为' + openid);
        //   }
        // })
      }
    })
    
    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              // 可以将 res 发送给后台解码出 unionId
              this.globalData.userInfo = res.userInfo
              // console.log(res.userInfo)
              wx.request({
                url: 'http://47.93.188.200/check?weixin=' + res.userInfo.nickName,
                success(res) {
                  console.log(res.data.res)
                  if(res.data.res == 0){
                    console.log(0)
                    wx.reLaunch({
                      url: '/pages/add/add',
                    })
                  } else if (res.data.res == 1) {
                    wx.showModal({
                      title: '提示',
                      content: '您的信息正在审核中，请联系管理员通过审核',
                      success: function () {
                        if (res.confirm) {
                          wx.navigateBack({
                            delta: -1
                          })
                        } else if (res.cancel) {
                          wx.navigateBack({
                            delta: -1
                          })
                        } else {
                          wx.navigateBack({
                            delta: -1
                          })
                        }
                      },
                    })
                    // wx.showToast({
                    //   title: '您的信息正在审核中，请联系管理员通过审核',
                    //   icon: 'none',
                    //   duration: 2000
                    // })
                  }else{
                    console.log("确认该成员信息")
                  }
                }
              })

              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (this.userInfoReadyCallback) {
                this.userInfoReadyCallback(res)
              }
            }
          })
        }
      }
    })
    // console.log(this.globalData.userInfo)

    /**
     * 获取教练信息
     */
    if (this.globalData.userPersonalInfo.coachId!=null) {
      // 获取教练信息
      var id = this.globalData.userPersonalInfo.coachId;
      /*wx.request({
        url: '',
      })*/
      var coachInfo = {
        "id": "00280021003",
        "name": "王三",
        "gender": 0,
        "tel": "13500000000",
        "licencePlate": "川AS110学",
        "avatarSrc": "https://develop.epearth.com/images/avatar.jpg",
        "students": 0
      }
      this.globalData.coachInfo = coachInfo
    }
    console.log(this.globalData.coachInfo)
    if (this.globalData.userPersonalInfo.userInfo != null){
      wx.request({
        url: 'http://47.93.188.200/userinfo?weixin=' + getApp().globalData.userInfo.nickName,
        success(res){
          console.log(res.data)
        }
      })
      var userPersonalInfo = {
        "name":res.data.name,
        "tel":res.data.phone
      }
      this.globalData.userPersonalInfo = userPersonalInfo
    }
  },

  globalData: {
    rootDomain: 'https://',
    sourceDomain: 'https://',
    appName: '腾飞约车',
    userInfo: null, // 微信返回的用户信息
    userPersonalInfo:null,
    coachInfo: null,
    dateList: []
  },
})