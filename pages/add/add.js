Page({
  data: {
    showModalStatus: false
  },
  powerDrawer: function (e) {
    var currentStatu = e.currentTarget.dataset.statu;
    this.util(currentStatu)
  },
  reg: function (e) {
    console.log(e.detail.value);

    wx.request({
      url: 'http://47.93.188.200/stuinfoadd?weixin=' + getApp().globalData.userInfo.nickName + '&name=' + e.detail.value['name'] + '&age=' + e.detail.value['age'] + '&phone=' + e.detail.value['phone'] + '&id=' + e.detail.value['id'] + '&place=' + e.detail.value['place'] ,
      success(res) {
        console.log(res.data)
        if(res.data.res == 0){
          wx.showToast({
            title: "申报失败，请勿重复申请",
            icon: 'none',
            duration: 2000
          })
        }else{
          wx.showToast({
            title: '申请成功，请联系管理员通过审核',
            icon: 'none',
            duration: 2000
          })
        }
      }
    })
  }
})