/*!
 * 作者author：刘旭，杨正宇
*/

Page({

  /**
   * 页面的初始数据
   */
  data: {
    trainingBill: [],
    untrainingBill: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    
    wx.request({
      url: 'http://47.93.188.200/finish?weixin=' + getApp().globalData.userInfo.nickName ,
      success(res) {
        that.setData({
          trainingBill: res.data
        });
        console.log(res.data)
      }
    }),
    wx.request({
      url: 'http://47.93.188.200/unfinish?weixin=' + getApp().globalData.userInfo.nickName,
      success(res) {
        that.setData({
          untrainingBill: res.data
        });
        console.log(res.data)
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },

  /**
   * 用户点击训练记录
   */
  clickBill: function (e) {
    wx.navigateTo({
      url: '../../training/details/details?id=' + e.currentTarget.dataset.id,
    })
    console.log('打开id为 ' + e.currentTarget.dataset.id + ' 的训练详情')
  },




  quxiao:function(e){
    var that = this
    wx.request({
      url: 'http://47.93.188.200/remove?weixin=' + getApp().globalData.userInfo.nickName + '&date=' + e.currentTarget.dataset.date + '&section='+ e.currentTarget.dataset.section,
      success(res) {
        //如果更新成功
        if (res.data[0].res == 1) {
          //提示状态修改成功
          wx.showToast({
            title: '取消成功'
          })
          this.onLoad()
        } else {
          wx.showToast({
            title: '取消失败',
            icon: 'none',
            duration: 2000
          })
        }
      }
    })
  },

  qiandao:function(e){
   // var that = this
    wx.request({
      url: 'http://47.93.188.200/signin?weixin=' + getApp().globalData.userInfo.nickName + '&date=' + e.currentTarget.dataset.date + '&section=' + e.currentTarget.dataset.section,
      success(res) {
          //如果更新成功
          if (res.data[0].res == 1) {
            //提示状态修改成功
            wx.showToast({
              title: '签到成功'
            })
            this.onLoad()
          }else{
            wx.showToast({
              title: '签到失败',
              icon: 'none',
              duration: 2000
            })
          }
      }
    })
  }
})