/*!
 * 作者author：刘旭，杨正宇
*/

const app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 以预定车位总数
    todayinfos: [],
    tomorrowinfos: [],
    adImage: '../../images/trainbgc.png',
    messageTitle: '提示',
    message: '消息内容',
    //日期
    timeList: [],
    //可预约天数
    yyDay: 2,
    //预约时间段
    hourList: [{ hour: "8:30-9:30", n: 1, isShow: true },
    { hour: "9:30-10.30", n: 2, isShow: true },
    { hour: "10:30-11:30", n: 3, isShow: true },
    { hour: "14:30-15:30", n: 4, isShow: true },
    { hour: "15:30-16:30", n: 5, isShow: true },
    { hour: "16:30-17:30", n: 6, isShow: true },
    { hour: "18:30-20:30", n: 7, isShow: true }
    ],
    //是否显示
    timeShow: false,
    timeShow2: false,
    //今天
    today: '',
    //明天
    tomorrow: '',
    // 当前id
    targetid: 0
  },
  
  showTimeModel: function () {
    var getdate = new Date()
    var thisDay = getdate.getFullYear() + '-' + (getdate.getMonth() + 1) + '-' + getdate.getDate()
    var self = this
    this.setData({
      timeShow: !this.data.timeShow,
      // chooseTime: this.data.timeList[0].date,
    });
    wx.request({
      url: 'http://47.93.188.200/detail?date=' + thisDay,
      success(res) {
        self.setData({
          todayinfos: res.data,
        })
      }
    })
  },
  showTimeModel2: function () {
    var getdate = new Date()
    var self = this
    var tomorrowDay = getdate.getFullYear() + '-' + (getdate.getMonth() + 1) + '-' + (getdate.getDate() + 1)
    this.setData({
      timeShow2: !this.data.timeShow2,
      // chooseTime: this.data.timeList[0].date,
    });
    wx.request({
      url: 'http://47.93.188.200/detail?date=' + tomorrowDay,
      success(res) {
        self.setData({
          tomorrowinfos: res.data,
        })
      }
    })
  },
  //点击外部取消
  modelCancel: function () {
    this.setData({
      timeShow: !this.data.timeShow,
      // chooseTime: this.data.timeList[0].date,
    });
  },
  modelCancel2: function () {
    this.setData({
      timeShow2: !this.data.timeShow2,
      // chooseTime: this.data.timeList[0].date,
    });
  },
  requestOrder: function () {
    console.log(this)
  },
  // 今天时间选择
  hourClick: function (e) {
    var getdate = new Date()
    var that = this;
    // // 时间不可选择
    // if (todayinfos.status==1) {
    //    return false;
    //  }
    var id = e.currentTarget.id
    // 退出界面
    this.setData({
      timeShow: !this.data.timeShow,
    });
    console.log(that.globalData)
    wx.request({
      url: 'http://47.93.188.200/order?weixin=' + getApp().globalData.userInfo.nickName + '&date=' + getdate.getFullYear() + '-' + (getdate.getMonth() + 1) + '-' + getdate.getDate() + '&sectionid=' + id,
      success(res) {
        if(res.data.res == 0){
          wx.showToast({
            title: res.data.message,
            icon: 'none',
            duration: 2000
          })
        }
        else{
          wx.showToast({
            title: '报名成功',
            icon: 'success',
            duration: 2000
          })
        }
        
      }
    })
  },
  // 明天时间选择
  hourClick2: function (e) {
    var getdate = new Date()
    var that = this;
    // 时间不可选择
    // if (tomorrowinfos.status==1) {
    //    return false;
    //  }
    var id = e.currentTarget.id
    // 退出界面
    this.setData({
      timeShow2: !this.data.timeShow2,
    });
    wx.request({
      url: 'http://47.93.188.200/order?weixin=' + getApp().globalData.userInfo.nickName + '&date=' + getdate.getFullYear() + '-' + (getdate.getMonth() + 1) + '-' + (getdate.getDate()+1) + '&sectionid=' + id,
      success(res) {
        if (res.data.res == 0) {
          wx.showToast({
            title: res.data.message,
            icon: 'none',
            duration: 2000
          })
        }
        else {
          wx.showToast({
            title: '报名成功',
            icon: 'success',
            duration: 2000
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 设置全局根域名
    var globalRootDomain = app.globalData.rootDomain;
    var globalSourceDomain = app.globalData.sourceDomain;
    this.setData({
      rootDomain: globalRootDomain,
      sourceDomain: globalSourceDomain,
    })
    // 设置logo完整src
    var adImageSrc = globalSourceDomain + '/images/agan.png';
    this.setData({
      adImage: adImageSrc
    })
    // 设置页面标题
    var globalAppName = app.globalData.appName;
    wx.setNavigationBarTitle({
      title: globalAppName,
    })
    // 设置data
    var self = this
    var getdate = new Date()
    var thisDay = getdate.getFullYear() + '-' + (getdate.getMonth() + 1) + '-' + getdate.getDate()
    var tomorrowDay = getdate.getFullYear() + '-' + (getdate.getMonth() + 1) + '-' + (getdate.getDate() + 1)
    self.setData({
      today: thisDay,
      tomorrow: tomorrowDay
    })
    
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },

  /**
   * 用户点击练车预约
   */
  clickReservation: function () {
    console.log('用户点击“练车预约”按钮')
    wx.navigateTo({
      url: 'reservation/reservation',
      success: function () { console.log('打开“练车预约”页面成功') },
      fail: function () { console.log('打开“练车预约”页面失败') },
    })
  },

  /**
   * 用户点击时间表
   */
  clickTimeTable: function() {
    console.log('用户点击' + this.data.button[0].text + '按钮')
    wx.navigateTo({
      url: 'timeTable/timetable',
      success: function () {  },
      fail: function () {  }, 
    })
  },

  /**
   * 提示消息
   */
  showMessage: function() {
    wx.showModal({
      title: this.data.messageTitle,
      content: this.data.message,
      showCancel: false,
      success: function (res) {
        if (res.confirm) {
          console.log('用户点击确定')
        } else {
          console.log('用户点击取消')
        }
      }
    })
  },

  /**
   * 开发中
   */
  clickNoPage: function() {
    wx.navigateTo({
      url: '../nopage/nopage',
    })
  }
})